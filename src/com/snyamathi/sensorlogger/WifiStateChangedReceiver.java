package com.snyamathi.sensorlogger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

public class WifiStateChangedReceiver extends BroadcastReceiver {
	private static final String LOG_TAG = "WifiStateChangedReceiver";
	private static WifiManager mWifiManager = null;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(LOG_TAG, "in onReceive() of wifiReceiver");
		int extraWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
		Log.i(LOG_TAG, "new wifi state: " + extraWifiState);
		if (extraWifiState == WifiManager.WIFI_STATE_ENABLED) {
			if (mWifiManager == null) {
				mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			}
			synchronized (mWifiManager) {
				 new WaitForIP().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "null");
			}
		}

	}
	
	private class WaitForIP extends AsyncTask<String, Integer, Integer> {

        @Override
        protected Integer doInBackground(String... arg0) {
        	synchronized (mWifiManager) {
				int ipAddress = 0;
				while ((ipAddress = mWifiManager.getConnectionInfo().getIpAddress()) == 0) {
					
				}
				WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
				String ip = null;
				ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
						(ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
						(ipAddress >> 24 & 0xff));
				Log.i(LOG_TAG, "wifi connected: ip address=" + ip);
				Log.i(LOG_TAG, "wifi connected: ssid=" + wifiInfo.getSSID());
				Log.i(LOG_TAG, "wifi connected: bssid=" + wifiInfo.getBSSID());
				Log.i(LOG_TAG, "wifi connected: rssi=" + wifiInfo.getRssi());
				Log.i(LOG_TAG, "wifi connected: link speed=" + wifiInfo.getLinkSpeed());
				DataCollectionService.lastWifiInfo = wifiInfo;
        	}
        	return 0;
        }
        
        @Override
        protected void onPostExecute(Integer result) {
                super.onPostExecute(result);
        }        
	}

}