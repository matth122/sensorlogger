package com.snyamathi.sensorlogger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

public class PhoneStateChangedReceiver extends BroadcastReceiver {
	private static final String LOG_TAG = "PhoneStateChangedReceiver";
	
	class MyPhoneStateListener extends PhoneStateListener {

		@Override
		public void onCellLocationChanged(CellLocation location) {
			GsmCellLocation gsm = (GsmCellLocation) location;
			Log.i(LOG_TAG, "new cell tower id=" + gsm.getCid());
			Log.i(LOG_TAG, "area code=" + gsm.getLac());
			
			DataCollectionService.lastCellInfo = gsm;
		}
		
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		MyPhoneStateListener phoneListener = new MyPhoneStateListener();
		TelephonyManager mTeleManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		mTeleManager.listen(phoneListener, PhoneStateListener.LISTEN_CELL_LOCATION);
	}
	
}